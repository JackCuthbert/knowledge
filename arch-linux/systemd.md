# Systemd

Systemd is the default system and service manager on Arch Linux.

## Manipulating services

Starting a service is as simple as `sudo systemctl start <ServiceName>`.

Using `enable` instead of `start` will ensure that the service is run when the system is first booted. This is usually used by services like `tlp`, `docker`, and `NetworkManager`.

Stopping is as you'd expect, `sudo systemctl stop <ServiceName>`.

### User services

It's also possible to have services that run as the current user using the `--user` flag. For example the `mpd` service can be enabled or started with `systemctl --user start mpd.service`.

## Mounts

Systemd can actually manipulate both local and networked shares. Store these `*.mount` unit files inside `/etc/systemd/system`.

Mounts **must** be named the same as the location they're mounted. For example mounting `/home/jack/Documents` should be named `home-jack-Documents.mount`.

### Local partitions

```
/etc/systemd/system/mnt-backups.mount
-------------------------------------

[Unit]
Description=Mount System Backups Directory

[Mount]
What=/dev/disk/by-uuid/86fef3b2-bdc9-47fa-bbb1-4e528a89d222
Where=/mnt/backups
Type=ext4
Options=defaults

[Install]
WantedBy=multi-user.target
```

### Network shares

#### Using NFS

Requires the installation of `nfs-utils` and a correctly configured target device.

```
/etc/systemd/system/mnt-backups.mount
-------------------------------------

[Unit]
Description=Audio NFS Mount
Requires=systemd-networkd.service
After=network-online.target
Wants=network-online.target

[Mount]
What=192.168.1.11:/volume1/Audio/Lossless
Where=/nfs/audio
Type=nfs
Options=_netdev,auto

[Install]
WantedBy=multi-user.target
```

#### Using SMB (Samba)

Using SMB requires the storage of credentials in a separate file and requires the installation of `cifs-utils`.

Credentials are stored in the file `/etc/samba/credentials/share` with the following permissions:

##### Files

```
Permissions User Group Name
drwx------  root root  /etc/samba/credentials
.rw-------  root root  /etc/samba/credentials/share
```

```
/etc/samba/credentials/share
----------------------------

username=share_username
password=share_password
```

```
/etc/systemd/system/mnt-audio-lossless.mount
--------------------------------------------

[Unit]
Description=Mount <Share Name> SMB Share at boot
Requires=systemd-networkd.service
After=network-online.target
Wants=network-online.target

[Mount]
What=//192.168.1.11/Audio/Lossless
Where=/mnt/audio/lossless
Options=credentials=/etc/samba/credentials/share,iocharset=utf8,rw,x-systemd.automount,gid=jack,uid=jack
Type=cifs
TimeoutSec=30

[Install]
WantedBy=multi-user.target
```

## Links

* [Mounting Partitions Using Systemd](https://oguya.ch/posts/2015-09-01-systemd-mount-partition/)
* [NFS mount via systemd](https://cloudnull.io/2017/05/nfs-mount-via-systemd/)