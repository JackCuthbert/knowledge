# 3. Install

Order the mirrorlist file how you like, I'll be putting Australian mirrors first

```bash
vim /etc/pacman.d/mirrorlist
```

Next, install the base system with some additional packages. For a more complete list of packages that I typically install see [my dotfiles](https://gitlab.com/JackCuthbert/dotfiles).

```bash
pacstrap /mnt base base-devel btrfs-progs zsh vim git sudo efibootmgr networkmanager network-manager-applet
```

> If this system has wifi, also include these: `wpa_supplicant dialog iw`

Finally, generate an `/etc/fstab` file and make recommended adjustments

```bash
genfstab -L /mnt >> /mnt/etc/fstab

# For all btrfs filesystems consider:
# - Change "relatime" to "noatime" to reduce wear on SSD
# - Adding "discard" to enable continuous TRIM for SSD
```

Next, we'll [configure the system](./configuration.html)