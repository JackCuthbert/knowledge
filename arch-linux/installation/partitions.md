# 2. Partitions

First we need to create some partitions on our disk, I prefer to use `fdisk`.

```bash
fdisk /dev/<devName>
```

## Partition tables

The following table should be used. Partition 1 is the boot partition, partition 2 will enable full disk encryption.

| partition | mount point        | type            | size         |
|-----------|--------------------|-----------------|--------------|
| 1         | `/boot`            | `ef00`          | `512M`       |
| 2         | `/`                | `8300`          | rest of disk |

## Create filesystems

After partitioning is done, create the boot filesystem on the first partition

```bash
mkfs.fat -F32 /dev/<devName>p1
```

Next, create the LUKs container using btrfs on the main partition and open it for use

```bash
cryptsetup luksFormat --type=luks2 /dev/<devName>p2
cryptsetup open /dev/<devName>p2 luks
mkfs.btrfs -L luks /dev/mapper/luks
```

Next you can create the btrfs subvolumes that are required for system installation

```bash
mount -t btrfs /dev/mapper/luks /mnt
btrfs subvolume create /mnt/@root
btrfs subvolume create /mnt/@var
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@swap # if using swap
```

## Mounting partitions

Finally we need to mount our subvolumes and boot partition

```bash
umount /mnt
mount -o subvol=@root /dev/mapper/luks /mnt
mkdir /mnt/{var,home,swap}
mount -o subvol=@var /dev/mapper/luks /mnt/var
mount -o subvol=@home /dev/mapper/luks /mnt/home
mount -o subvol=@swap /dev/mapper/luks /mnt/swap # if using swap
```

Create an 8G swapfile if you're using swap (requires linux 5+, check [this guide](https://fogelholk.io/installing-arch-with-lvm-on-luks-and-btrfs/) for more details)

```
truncate -s 0 /mnt/swap/swapfile
chattr +C /mnt/swap/swapfile
dd if=/dev/zero of=/mnt/swap/swapfile bs=1M count=8192 status=progress
chmod 600 /mnt/swap/swapfile
mkswap /mnt/swap/swapfile
swapon /mnt/swap/swapfile
```

Mount boot partition

```bash
mkdir /mnt/boot
mount /dev/<devName>p1 /mnt/boot
```

Next we'll [install the system](./install.html).