# Arch Linux Install Guide

This is my personal guide that I've used to install Arch on my various systems. Your mileage may vary when using this guide, always refer to the official documentation if you get stuck.

Much of this guide is based on [this amazing gist by njam](https://gist.github.com/njam/85ab2771b40ccc7ddcef878eb82a0fe9). 

[Get started](./get-started.html) (or use the arrows at the bottom)

## Links

1. [Download Arch Linux](https://www.archlinux.org/download/)
2. [USB flash installation media](https://wiki.archlinux.org/index.php/USB_flash_installation_media)
3. [Installation Guide](https://wiki.archlinux.org/index.php/Installation_Guide)
4. [GitHub Gist](https://gist.github.com/njam/85ab2771b40ccc7ddcef878eb82a0fe9)