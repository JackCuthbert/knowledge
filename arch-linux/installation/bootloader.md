# 5. Bootloader

Setup [systemd-boot](https://wiki.archlinux.org/index.php/Systemd-boot)

```bash
bootctl --path=/boot install
```

Enable microcode updates

```bash
pacman -S amd-ucode # or intel-ucode
```

Get the luks-uuid

```bash
cryptsetup luksUUID /dev/<devName>p2
```

Use the luks-uuid to create the bootloader entry in `/boot/loader/entries/arch.conf`

```
title   Arch Linux
linux   /vmlinuz-linux
initrd  /amd-ucode.img # or /intel-ucode.img
initrd  /initramfs-linux.img
options rw luks.uuid=<uuid> luks.name=<uuid>=luks root=/dev/mapper/luks rootflags=subvol=@root
```

> An quick way to do this without clipboard support is to use the `:r! cryptsetup luksUUID /dev/<devName>p2` command within vim

Set default bootloader entry and options in `/boot/loader/loader.conf`

```
default	arch
timeout 3 # if needed
```

Finally, reboot!

```bash
exit
reboot
```

At this point you should have a working Arch Linux installation and any desktop customisations are up to you. See [my dotfiles](https://gitlab.com/JackCuthbert/dotfiles) or [Desktop Experience](../desktop-experience.html) for info on how I set up my system.