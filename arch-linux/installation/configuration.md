# 4. Configuration

Enter the new system with chroot

```bash
arch-chroot /mnt
```

Set the timezone and sync it to the system clock

```
rm /etc/localtime
ln -s /usr/share/zoneinfo/Australia/Melbourne /etc/localtime
hwclock --systohc
```

Generate the required locales

```bash
vim /etc/locale.gen	# Uncomment desired locales, e.g. "en_US.UTF-8", "en_AU.UTF-8"
locale-gen
```

Set the desired locale

```bash
echo 'LANG=en_AU.UTF-8' > /etc/locale.conf
```

Set desired keymap and font

```bash
echo 'KEYMAP=us' > /etc/vconsole.conf
echo 'FONT=latarcyrheb-sun32' >> /etc/vconsole.conf
```

Set a hostname

```
echo '<hostname>' > /etc/hostname
```

Add that hostname to `/etc/hosts`

```
127.0.0.1	localhost
::1		    localhost
127.0.1.1	<hostname>.localdomain <hostname>
```

Set a root password

```bash
passwd
```

Add a real user

```bash
useradd -m -g users -G wheel -s /bin/zsh <username>
passwd <username>
```

Allow this user to use `sudo` by uncommenting the wheel group line after running the `visudo` command

```
%wheel ALL=(ALL) ALL
```

Configure mkinitcpio with modules needed for the initrd image

```bash
vim /etc/mkinitcpio.conf
# Change: HOOKS=(base systemd autodetect modconf block keyboard sd-vconsole sd-encrypt filesystems)
```

Regenerate initrd image

```bash
mkinitcpio -p linux
```

Enable the NetworkManager service for networking support on boot

```bash
systemctl enable NetworkManager.service
```

Next, we'll [configure the bootloader](./bootloader.html)
