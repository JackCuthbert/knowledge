# 1. Get Started

After booting into archiso, set a large font to see what you're doing. This is especially useful for the tiny XPS display.

```bash
setfont latarcyrheb-sun32
```

If this is a device with wifi, connect to the internet now

```bash
wifi-menu
```

Set the time

```bash
timedatectl set-ntp true
```

At this point we need to create some partitions, refer to [partitions](./partitions.html).