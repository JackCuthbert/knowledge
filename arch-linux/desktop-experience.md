# Desktop Environment

With i3-gaps!

## Installation

`sudo pacman -S i3-gaps i3status` and that's it!

## Desktop experience

i3 is only a window manager and does not provide a fully-featured desktop experience (in my opinion). The following is some software that I use to flesh out my desktop inspired by [rstacruz/ricefiles](https://github.com/rstacruz/ricefiles/blob/master/docs/desktop_environment_packages.md).

**Legend**

* 🔥 - You need this
* ✨ - Nice to have
* 🚨 - This will suck up your time, avoid!

### Desktop

|    | Name                                                                                       | Description                                                                                                                                                                                | Location |
|----|--------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| 🔥 | **[compton](https://www.archlinux.org/packages/?q=compton)**                               | X display compositor. This will fix some tearing issues.                                                                                                                                   | official |
| 🔥 | **[dunst](comptonhttps://www.archlinux.org/packages/?q=dunst)**                            | Notification daemon. This shows notifications in your desktop.                                                                                                                             | official |
| 🔥 | **[feh](https://www.archlinux.org/packages/?q=feh)**                                       | Sets wallpapers. Simple, CLI stuff. (alternative: [nitrogen](https://www.archlinux.org/packages/?q=nitrogen) has a GUI.)                                                                   | official |
| 🔥 | **[i3status](https://www.archlinux.org/packages/?q=i3status)**                             | Desktop panel. A simple configurable bar, usually the default for i3.                                                                                                                      | official |
| 🔥 | **[lxappearance](https://www.archlinux.org/packages/?q=lxappearance)**                     | Allows you to pick GTK themes, cursors, and so on.                                                                                                                                         | official |
| 🔥 | **[maim](https://www.archlinux.org/packages/?q=maim)**                                     | CLI screenshot tool (similar to MacOS's). I bind a command to `mod-ctrl-4` for a similar experience. Works well with [xclip](https://www.archlinux.org/packages/?q=xclip)                  | official |
| 🔥 | **[network-manager-applet](https://www.archlinux.org/packages/?q=network-manager-applet)** | Choosing wifi networks from the systray. (alternatives: `nmtui` for terminal, which already comes with [networkmanager](https://www.archlinux.org/packages/?q=networkmanager) by default.) | official |
| 🔥 | **[pavucontrol](https://www.archlinux.org/packages/?q=pavucontrol)**                       | A GUI to manage pulseaudio sound streams. (Similar to `sudo alsamixer`)                                                                                                                    | official |
| 🔥 | **[redshift](https://www.archlinux.org/packages/?q=redshift)**                             | Remove blue light emitted by displays at night.                                                                                                                                            | official |
| 🔥 | **[rofi](https://www.archlinux.org/packages/?q=rofi)**                                     | Launcher. It's a bit like "Alfred for Linux".                                                                                                                                              | official |
| ✨  | **[fff](https://www.archlinux.org/packages/?q=fff)**                                      | Terminal based file manager with Vim bindings.                                                                                                                                             | official |
| ✨  | **[thunar](https://www.archlinux.org/packages/?q=thunar)**                                | GTK based file manager.                                                                                                                                                                    | official |
| 🚨️ | **[polybar](https://www.archlinux.org/packages/?q=polybar)**                               | Desktop panel. This is super nice to look at but configuration is a time sink.                                                                                                             | official |

### Theming

|   | Name                                                                                   | Description                   | Location |
|---|----------------------------------------------------------------------------------------|-------------------------------|----------|
| ✨ | **[bibata-cursor-theme](https://aur.archlinux.org/packages/bibata-cursor-theme/)**     | Nice X cursor theme.          | AUR      |
| ✨ | **[ant-dracula-gtk-theme](https://aur.archlinux.org/packages/ant-dracula-gtk-theme/)** | A slick Dracula theme for GTK | AUR      |

### Bluetooth

|   | Name                                                         | Description                                          | Location |
|---|--------------------------------------------------------------|------------------------------------------------------|----------|
| ✨ | **[blueman](https://www.archlinux.org/packages/?q=blueman)** | GTK GUI for managing bluetooth devices and adapters. | official |

### Lock

|    | Name                                                          | Description                                                          | Location |
|----|---------------------------------------------------------------|----------------------------------------------------------------------|----------|
| 🔥 | **[i3lock](https://www.archlinux.org/packages/?q=i3lock)**    | Must have to lock your screen. Requires some scripting.              | official |
| 🔥 | **[xidlehook](https://aur.archlinux.org/packages/xidlehook)** | Replacement for xautolock to run scripts at intervals of inactivity. | AUR      |