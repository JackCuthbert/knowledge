# Aura

Aura is AUR helper of my choice. It provides a very simple set of commands to easily install, remove, downgrade, and update packages from both AUR and the official repo.

## Installation

```bash
# install required dependencies
sudo pacman -S binutils fakeroot

# create a place to store the PKGBUILD file
mkdir aura-bin && cd aura-bin

# get the latest PKGBUILD file from AUR
wget -O PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=aura-bin

# install
makepkg --install
```

## Usage

Complete documentation for Aura usage can be found in their brilliantly written man page: `man aura`.

* Update installed packages: `sudo aura -Au`
* Install a package and remove make dependencies: `sudo aura -Aa <packagename>`
* Update existing packages: `sudo aura -Au`

## Links

* [Aura on GitHub](https://github.com/aurapm/aura)