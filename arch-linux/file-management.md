# File Management

I use [fff](https://github.com/dylanaraps/fff) for my file management primarily. Alternatively, [ranger](https://github.com/ranger/ranger) is a good option.

## Installation

```bash
# fff
sudo pacman -S fff w3m xdotool
```

> Note: `w3m` and `xdotool` are required for displaying images in the terminal

## Usage

fff uses typical vim-like keybindings for navigation, some useful default bindings are:

* `.` - toggle hidden files
* `~` - go to home directory
* `x` - show file attributes/properties
* `i` - preview image
* `r` - rename file
* `f` - new file
* `n` - new dir
* `r` - rename
