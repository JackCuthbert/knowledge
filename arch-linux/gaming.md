# Gaming on Arch Linux

Gaming on Linux is getting better and better every day. This is a collection of configurations that I've found while using Arch.

## Graphics Drivers & Gaming

Install the `nvidia` and `nvidia-settings` packages. EZ PZ.

### Issues

Gaming under Linux can be problematic with Nvidia drivers. I've managed to find a good balance for my current system setup.

Some things I've tried to eliminate tearing and stutter in games and on the desktop:

1. compton runnng w/ vsync off + full composition pipeline = hectic input lag, no tearing
2. compton not running + full composition pipeline = same as 1
3. no composition pipeline + compton vsync on = serious stutter, low(er) input lag
4. no composition pipeline + compton vsync off = tearing city, unplayable
5. no composition pipeline + compton not running = same as 4
6. a bunch of different compton configurations, too many combinations to list here

I've settled on **no compositor** and **no composition pipeline** coupled with the mouse acceleration config below. It seems to give the best results when it comes to tearing, input lag, and stutter.

### Bluetooth Xbox One S Controller

Install [this awesome linux driver](https://github.com/atar-axis/xpadneo/) to get out of the box support for using the Xbox controller via bluetooth.

## Mouse Sensitivity

I had a lot of trouble working out how to completely disable software mouse acceleration. Finally worked out that **using the `libinput` driver** solves literally all of my problems.

```
/etc/X11/xorg.conf.d/20-mouse-acceleration.conf
-----------------------------------------------

Section "InputClass"
  Identifier "My Mouse"
  Driver "libinput"
  MatchIsPointer "yes"
  Option "AccelProfile" "flat"
EndSection
```

## Steam

Steam works very well on Arch Linux. Install the `steam` and `steam-fonts` packages.

> If steam fails to start, make sure that `lib32-nvidia-utils` is installed.

These packages will create a `.desktop` file in `/usr/share/applications`, remember to set `Hidden=true` on the native desktop file.

## Links

* [Phoronix](http://phoronix.com/)
* [r/linux_gaming](https://www.reddit.com/r/linux_gaming)