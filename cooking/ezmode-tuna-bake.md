# Ezmode Tuna Back

Here's how I make Tuna Bake.

## Ingredients

* 500g Leggo's (or Dolmio) Tuna Bake Sauce jar
* 2-3 cups of tube-like pasta (rigatoni, penne, etc)
* 425g tuna
* 250g block of tasty cheese

## Instructions

1. Cook pasta, preheat oven to 180deg
3. Once pasta is cooked, strain and pour into [oven-safe dish](https://pyrex.com.au/products/pyrex-easy-grab-20cm-square-baker/)
4. Mix in can of tuna and tuna bake sauce
5. Grate enough cheese to cover the top of the entire dish
6. Cook in oven on fan-forced grill for 20 minutes
7. Once cheese is starting to brown after 20 minutes, take out to cool
8. Serve
