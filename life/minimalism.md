# Minimalism

> Eliminate all that is unnecessary from my life.

* Better financial security
* Reduced stress levels
* Focus on what's important

## Cleaning and organising

1. Clean _less_, more frequently
2. Create piles of "stuff" that no longer adds value, remove it all at once
3. Create systems for storing essential items

## Buying stuff

1. Before buying, consider the value the product will add
2. Buy fewer, higher quality things that last longer ([Massdrop](https://www.massdrop.com/?referer=N5TJS6), [Apple](https://apple.com/au))
3. Sell or give away what you have _before_ upgrading
4. Avoid eBay, Aliexpress, IKEA, etc
5. Ignore retail employee upsell, understand the product you're buying

## Selling stuff

After many **horrible** experiences trying to sell my stuff on [Gumtree](http://gumtree.com.au/) I've decided to:

1. Donate stuff instead
2. Gift stuff to friends or family instead
3. Sell stuff to friends or family instead at a _really good_ price*

\* By _really good_ price, I mean practically free. Once I'm selling something I've decided that it no longer adds value to my life. Unless I need to recoup the cost of the item (which is likely if it was a big purchase) I would prefer to help my friends and family out with a great deal.

## Recycling

* [Apple GiveBack](https://www.apple.com/au/trade-in/) will recycle old Apple devices for free
* [Appliances Online](https://www.appliancesonline.com.au/) will take away old items for free upon delivery
* [Harvey Norman](https://www.harveynorman.com.au/customer-service/tv-recycling) will recycle an old TV for you

## Links

* [The Minimalists Podcast](https://www.theminimalists.com/podcast/)
* [Matt D'Avella: What minimalism really looks like...](https://www.youtube.com/watch?v=jrf_dMnatW0)