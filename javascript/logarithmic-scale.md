# Logarithmic Scale

Tweaked for purpose in a horizontal bar graph to visualise relations between things being selected from a list.

## Parameters

1. Provide the number you're mapping to the scale as `value`.
2. Provide the absolute minimum and maximum values for the data set
3. Optionally provide the precision to calculate mappings to

```typescript
export function mapValueToLogScale (value: number, minVal: number, maxVal: number, precision = 2) {
  // pad each value by 10% so the minimum and maximum aren't right up against
  // the constraints
  const paddedValue = value + (value * 0.1)

  // The scale will be between 0 and 1
  const minPos = 0
  const maxPos = 1

  // The result should be between the minimum and maximum values
  const minV = Math.log(minVal)
  const maxV = Math.log(maxVal)

  // calculate adjustment factor
  const scale = (maxV - minV) / (maxPos - minPos)

  return ((Math.log(paddedValue) - minV) / scale + minPos).toPrecision(precision)
}
```

> Note that each value is padding 10% to avoid ending up with 0 values at the bottom of the scale. This led to some misleading rendering behaviour in our application.

Confidently borrowed from <https://stackoverflow.com/a/846249/3796665>
