# Node Crypto

Learnings from working with crypto in Node.js.

## Generating strings

Using `nanoid` you can create "secure" strings.

```bash
npx nanoid-cli --size 32
```

## Hashing passwords

[bcrypt](https://github.com/kelektiv/node.bcrypt.js) is a library that helps you hash passwords without maintaining a secret key.

```js
import bcrypt from 'bcrypt'

async function someAuthFunction () {
  // Compare the users password hash to the incoming one
  const match = await bcrypt.compare(password, passwordFromDb)

  // Create a hash from the incoming plain text password with 10 salt rounds
  const newHash = await bcrypt.hash(plaintext, 10)
}
```