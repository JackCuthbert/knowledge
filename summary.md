# Summary

* [Introduction](readme.md)
* Meta
  * [Knowledge Base Rules](meta/knowledge-base-rules.md)
* Software
  * [Docker](software/docker.md)
  * [Git](software/git.md)
  * [jq](software/jq.md)
  * [mpd](software/mpd.md)
  * [Neovim](software/neovim.md)
  * [Nginx Proxy](software/nginx-proxy.md)
  * [Pihole](software/pihole.md)
  * [Syncthing](software/syncthing.md)
  * [Unix](software/unix.md)
* Rust
  * [Getting Started](rust/getting-started.md)
* JavaScript
  * [Async Express Middleware](javascript/async-express-middleware.md)
  * [Debounce Handler](javascript/debounce-handler.md)
  * [Easing Functions](javascript/easing-functions.md)
  * [Logarithmic Scale](javascript/logarithmic-scale.md)
  * [Node Crypto](javascript/node-crypto.md)
  * [Slack Request Verification](javascript/slack-request-verification.md)
* [Arch Linux](arch-linux/readme.md)
  * [Installation](arch-linux/installation/readme.md)
    * [1. Get Started](arch-linux/installation/get-started.md)
    * [2. Partitions](arch-linux/installation/partitions.md)
    * [3. Install](arch-linux/installation/install.md)
    * [4. Configuration](arch-linux/installation/configuration.md)
    * [5. Bootloader](arch-linux/installation/bootloader.md)
  * [Aura](arch-linux/aura.md)
  * [Desktop Experience](arch-linux/desktop-experience.md)
  * [File Management](arch-linux/file-management.md)
  * [Gaming](arch-linux/gaming.md)
  * [Private Internet Access](arch-linux/private-internet-access.md)
  * [Systemd](arch-linux/systemd.md)
* Life
  * [Minimalism](life/minimalism.md)
* Cooking
  * [Ezmode Bolognese](cooking/ezmode-bolognese.md)
  * [Ezmode Tuna Bake](cooking/ezmode-tuna-bake.md)