# Deprecated

This knowledge repo is now out of date! I've rebuilt it into my personal website
at [jackcuthbert.dev](https://jackcuthbert.dev/knowledge)

---

# Knowledge

> 💡 document everything

Based on other knowledge collections, see [RichardLitt/meta-knowledge](https://github.com/RichardLitt/meta-knowledge).

## How?

This notebook uses a self-hosted version of the free and open source software [GitBook](https://www.gitbook.com/) ([GitHub](https://github.com/GitbookIO/gitbook)).

All documents are written using valid markdown syntax and are built using GitBook into a static website to enable a clean reading experience and **searchable** content.

## Where?

This project is currently built and deployed using GitLab's CI/CD platform ([view builds](https://gitlab.com/JackCuthbert/knowledge/pipelines)) to a [GitLab pages URL](https://jackcuthbert.gitlab.io/knowledge). The source is also available [on GitLab](https://gitlab.com/JackCuthbert/knowledge).

### Requirements

* [Node 8+](https://nodejs.org/en/)
* [Text](https://code.visualstudio.com/) [editor](https://neovim.io/)

#### Install, start, build

```bash
# install dependencies
npm install

# start a live-reloading server to preview notes
# runs at http://localhost:9001/
npm start

# build the static site to public/
npm run build
```
