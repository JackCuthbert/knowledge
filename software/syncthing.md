# Syncthing

Syncthing is a self-hosted, open source version of popular file synchronisation services like Dropbox, Google Drive, or OneDrive.

## Installation

* macOS: `brew install syncthing`
* Arch Linux: `sudo pacman -S syncthing`, `systemctl --user enable syncthing`

### docker-compose.yml

```yml
syncthing:
   image: linuxserver/syncthing
   container_name: 'syncthing'
   restart: unless-stopped
   environment:
     PUID: 1000
     PGID: 1000
   ports:
     - '8384:8384'
     - '22000:22000'
     - '21027:21027/udp'
   volumes:
     - ./syncthing:/config
```

## `.stignore`

Syncthing tries to be helpful when creating new shares by just including everything.

For new shares, use the following ignores.

```
// At least for now, these prevent Syncthing from transferring data that's
// going to be thrown out anyway once the download is finished and the
// file is renamed. Things may change when Syncthing gets smarter.
//
// Incomplete Downloads

// Firefox downloads and other things
*.part

// Chrom(ium|e) downloads
*.crdownload

// Temporary / Backup Files
*~
.*.swp

// OS-generated files (macOS)
.DS_Store
.Spotlight-V100
.Trashes
._*
.localized
Icon

// OS-generated files (Windows)
desktop.ini
ehthumbs.db
Thumbs.db

// BTSync files
.sync
*.bts
*.!Sync
.SyncID
.SyncIgnore
.SyncArchive
*.SyncPart
*.SyncTemp
*.SyncOld

// Synology files
@eaDir
```