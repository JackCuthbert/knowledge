# MPD

Music player deamon

## Setup

Install the required software:

```bash
sudo aura -S mpd ncmpcpp mpdscribble
sudo aura -Aa mpd-mpris
```

Set up the required files. See my dotfiles for [mpd](https://gitlab.com/JackCuthbert/dotfiles/blob/master/config/mpd.conf)/[ncmpcpp](https://gitlab.com/JackCuthbert/dotfiles/blob/master/config/ncmpcpp.conf) here.

```bash
touch ~/.config/mpd/{database,log,pid,state,sticker.sql}
```

Configure [mpdscribble](https://github.com/MusicPlayerDaemon/mpdscribble/) (~/.mpdscribble/mpdscribble.conf) with your last.fm credentials:

```
username = LAST_FM_USERNAME
password = LAST_FM_PASSWORD
```

Start the services (use `enable` to run them at boot)

```bash
systemctl --user start mpd mpdscribble mpd-mpris
```

## Links

* [Ncmpcpp on Arch Linux Wiki](https://wiki.archlinux.org/index.php/Ncmpcpp)
* [MPD on Arch Linux Wiki](https://wiki.archlinux.org/index.php/Music_Player_Daemon)
