# Unix

A collection of unix tools that I've used either frequently or infrequently along with some nice aliases or example commands.

## Zsh

I use zsh for my shell. The community and plugins supporting it are fantastic and well documented.

## Zplug

Zplug is a plugin manager for zsh, it's super fast and works really well with an easy syntax to follow.

## Pure

Pure is a clean, minimal, and pretty looking prompt for zsh. It shows git branches and changes, SSH usernames, and command execution times.

I typically install this with zplug with the following.

```zsh
zplug "mafredri/zsh-async", \
  from:github,\
  use:"async.zsh"

zplug "sindresorhus/pure", \
  use:pure.zsh, \
  from:github, \
  as:theme
```

## Direnv

Direnv lets you define environment variables locally to a folder container a `.envrc` file instead of polluting your global `.zshrc`.

It's available via the AUR on Arch Linux or Homebrew on macOS. Once it's installed, add the following somewhere in your `.zshrc`.

```zsh
eval "$(direnv hook zsh)"
```

## Thefuck

Thefuck is a utility that will repeat the last command you made _correctly_ if you made a typo. ie.

```bash
$ git pshu

git: 'pshu' is not a git command. See 'git --help'.
The most similar command is
        push

$ fuck
git push

```

I typically install this with zplug and my OS package manager (named `thefuck`).

```zsh
zplug "plugins/thefuck", \
  from:oh-my-zsh
```


## Links 

* [Direnv](https://direnv.net/)
* [Pure on GitHub](https://github.com/sindresorhus/pure)
* [thefuck on GitHub](https://github.com/nvbn/thefuck)
* [Zplug on GitHub](https://github.com/zplug/zplug)
