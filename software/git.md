# Git

## Branching

### Renaming a Git branch both locally and on the remote host.

1. Rename branch locally
  ```
  git branch -m old_branch new_branch
  ```
2. Delete the old branch from the remote
  ```
  git push origin :old_branch
  ```
3. Push the new branch to the remote
  ```
  git push -u origin new_branch
  ``` 