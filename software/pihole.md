# Pihole

> A black hole for Internet advertistments.

I use pi-hole for network-wide ad and tracker blocking. [GitHub](https://github.com/pi-hole/pi-hole)

## Extra hosts

To add extra hosts to `/etc/hosts`, use the `extra_hosts` field in the docker compose file:

```yaml
pihole:
  image: pihole/pihole:latest
  extra_hosts:
    # Adds minecraft.server.home -> 192.168.1.123 to the hosts file
    - 'minecraft minecraft.server.home:192.168.1.123'
```

## Full compose file

```yaml
version: '3.2'
services:
  pihole:
    image: pihole/pihole:latest
    ports:
      - '53:53/tcp'
      - '53:53/udp'
      # - "67:67/udp" # enable 67 to run dhcp server
    environment:
      # This must match the IP of the host running this container
      ServerIP: 192.168.1.123
      TZ: Australia/Melbourne
      # Primary and secondary upstream DNS servers
      DNS1: 1.1.1.1
      DNS2: 9.9.9.9

      # Set the web ui password
      WEBPASSWORD: 'CHANGE_ME'

      # Use the jwilder proxy to access the web ui
      VIRTUAL_HOST: pihole.server.home
    extra_hosts:
      # Add any extra hosts that should resolve to any local services or
      # alternative ips
      - 'minecraft minecraft.server.home:192.168.1.123'
    volumes:
      # Exposing these to the host make it simpler to dig through logs or
      # configuration files.
      - ./pihole:/etc/pihole/
      - ./pihole/pihole.log:/var/log/pihole.log
      - ./pihole_dnsmasq:/etc/dnsmasq.d/
```

### Resources

* [The Big Blocklist Collection](https://firebog.net/)